﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tinker.CodeRush.Notation.XamarinSimpleMVVM
{
    public class ListViewBaseTypeAttribute:Attribute
    {
        Type _BaseType;
        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewBaseTypeAttribute"/> class.
        /// </summary>
        /// <param name="baseType"></param>
        public ListViewBaseTypeAttribute(Type baseType)
        {
            _BaseType = baseType;
        }
    }
}
