﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tinker.CodeRush.Notation.XamarinSimpleMVVM
{
    public class EditViewBaseTypeAttribute:Attribute
    {
        Type _BaseType;
        /// <summary>
        /// Initializes a new instance of the <see cref="EditViewBaseType"/> class.
        /// </summary>
        /// <param name="baseType"></param>
        public EditViewBaseTypeAttribute(Type baseType)
        {
            _BaseType = baseType;
        }
    }
}
