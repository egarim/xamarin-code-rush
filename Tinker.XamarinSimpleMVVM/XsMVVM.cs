using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.CodeRush.Core;
using DevExpress.CodeRush.PlugInCore;
using DevExpress.CodeRush.StructuralParser;
using System.IO;
using CRAttribute = DevExpress.CodeRush.StructuralParser.Attribute;

namespace Tinker.XamarinSimpleMVVM
{
    public partial class XsMVVM : StandardPlugIn
    {
      
    
    
        //JM
        private ElementBuilder _builder;
        private CRAttribute _IsView;
        //Defautl page and cell types
        private TypeReferenceExpression _CellBaseType = new TypeReferenceExpression("ViewCell");
        private TypeReferenceExpression _ListViewBaseType = new TypeReferenceExpression("ContentPage");
        private TypeReferenceExpression _EditViewBaseType = new TypeReferenceExpression("ContentPage");
        private string _ViewName;
        private TextDocument _textDocument;

        // DXCore-generated code...
        #region InitializePlugIn
        public override void InitializePlugIn()
        {
            base.InitializePlugIn();
            //registerGenerateXamarinView();
            //RegisterGenerateXamarinListView();
            CreateXamarinView();
        }
        #endregion
        #region FinalizePlugIn
        public override void FinalizePlugIn()
        {
            base.FinalizePlugIn();
        }
        #endregion
        #region Implment


        public void registerGenerateXamarinView()
        {
            CodeProvider GenerateXamarinView = new CodeProvider(components);
            ((ISupportInitialize)(GenerateXamarinView)).BeginInit();
            GenerateXamarinView.ProviderName = "GenerateXamarinView"; // Should be Unique
            GenerateXamarinView.DisplayName = "Generate Xamarin View";
            GenerateXamarinView.CheckAvailability += GenerateXamarinView_CheckAvailability;
            GenerateXamarinView.Apply += GenerateXamarinView_Apply;
            ((ISupportInitialize)(GenerateXamarinView)).EndInit();
        }

        #endregion

        public void CreateXamarinView()
        {
            CodeProvider GenerateXamarinView = new CodeProvider(components);
            ((ISupportInitialize)(GenerateXamarinView)).BeginInit();
            GenerateXamarinView.ProviderName = "CreateXamarinView"; // Should be Unique
            GenerateXamarinView.DisplayName = "Generate Xamarin Views";
            GenerateXamarinView.CheckAvailability += CreateXamarinView_CheckAvailability;
            //GenerateXamarinView.Apply += CreateXamarinView_Apply;
            GenerateXamarinView.Apply += GenerateXamarinView_Apply;
            ((ISupportInitialize)(GenerateXamarinView)).EndInit();
        }
        #region ImplementNext

        //private void CreateXamarinView_Apply(object sender, ApplyContentEventArgs ea)
        //{
        //    var TriggerClass = CodeRush.Source.ActiveClass;
        //    //var ViewAttribute = (from SP.Attribute att in TriggerClass.Attributes
        //    //                     where att.Name == "View"
        //    //                     select att).FirstOrDefault();
        //    var ViewAttribute = TriggerClass.Attributes[0];
        //    // Read the argument being passed to the attribute.


        //    var FirstArg = _IsView.Arguments[0];

        //    // Cast the first argument to a TypeOfExpression.
        //    var TypeOf = (FirstArg as TypeOfExpression);
        //    // Get the TypeReference from the TypeOfExpression.
        //    TypeReferenceExpression TypeReference = TypeOf.TypeReference;

        //    // Locate the ModelClass's definition.
        //    IElement ModelClassDeclaration = TypeReference.GetDeclaration();
        //    Class GetClass = TypeReference.GetClass();
        //    Class FindClass = CodeRush.Source.FindType(ModelClassDeclaration.Name) as Class;
        //    Class FromTypeOf = TypeOf.GetClass();
        //    var Customer = ModelClassDeclaration as Class;
        //    // Locate the ModelClass's definition.


        //    string ListViewClass = string.Empty;

        //    SolutionElement activeSolution = CodeRush.Source.ActiveSolution;
        //    foreach (ProjectElement project in activeSolution.AllProjects)
        //        foreach (SourceFile sourceFile in project.AllFiles)
        //            foreach (TypeDeclaration typeDeclaration in sourceFile.AllTypes)
        //            {
        //                var TestClass = typeDeclaration.GetClass();
        //                if (TestClass.Name == ModelClassDeclaration.Name)
        //                {
        //                    ListViewClass = CreateListViewClass2(TestClass);
        //                }
        //            }



        //    // Locate the end of the TriggerClass.
        //    var LineAfterClass = new SourcePoint(TriggerClass.EndLine + 1, 1);

        //    // Emit new code at the end of the class rather than in a new file.        
        //    ea.TextDocument.InsertText(LineAfterClass, ListViewClass);


        //    // Get the TypeReference from the TypeOfExpression.
        //    _builder = ea.NewElementBuilder();
        //    _textDocument = ea.TextDocument;



        //    string SourceFolder = _textDocument.GetFolderName();

        //    // Get Class


        //    string ViewNamespace = _textDocument.ProjectElement.DefaultNamespace + ".Views";
        //}

        #endregion
        private void CreateXamarinView_CheckAvailability(object sender, CheckContentAvailabilityEventArgs ea)
        {
            var ThisClass = ea.Element as Class;
            if (ThisClass == null)
                return;

            _IsView = ThisClass.FindAttribute("ViewModel");

            if (_IsView == null)
                return;

            ea.Available = true;

            AssignmentExpression ViewNamParameter = _IsView.Arguments[0] as AssignmentExpression;
            _ViewName = ViewNamParameter.RightSide.ToString().Replace("\"", "");


        }

        #region Implement

        private void GenerateXamarinListView_CheckAvailability(object sender, CheckContentAvailabilityEventArgs ea)
        {
            var ThisClass = ea.Element as Class;
            if (ThisClass == null)
                return;




            DevExpress.CodeRush.StructuralParser.Attribute IsModel = ThisClass.FindAttribute("ModelClass");
            if (IsModel != null)
            {
                ea.Available = true;
            }



        }
       
        private void RegisterGenerateXamarinListView()
        {
            CodeProvider GenerateXamarinView = new CodeProvider(components);
            ((ISupportInitialize)(GenerateXamarinView)).BeginInit();
            GenerateXamarinView.ProviderName = "XamarinListView"; // Should be Unique
            GenerateXamarinView.DisplayName = "Xamarin List View";
            GenerateXamarinView.CheckAvailability += GenerateXamarinListView_CheckAvailability;
            GenerateXamarinView.Apply += GenerateXamarinView_Apply;
            ((ISupportInitialize)(GenerateXamarinView)).EndInit();
        }
        #endregion
        
        
       
        private void GenerateXamarinView_CheckAvailability(Object sender, CheckContentAvailabilityEventArgs ea)
        {
            var ThisClass = ea.Element as Class;





            if (ThisClass == null)
                return;



            ITypeElement inherits = ThisClass.GetBaseType();

            ea.Available = true;
        }
        private static TypeReferenceExpression GetTypeRefence(CRAttribute item)
        {
            return (item.Arguments[0] as TypeOfExpression).TypeReference;
        }
        private void GenerateXamarinView_Apply(Object sender, ApplyContentEventArgs ea)
        {
            _builder = ea.NewElementBuilder();
            _textDocument = ea.TextDocument;
           

            string SourceFolder = _textDocument.GetFolderName();

            //TODO Create folder for the views
            //DevExpress.CodeRush.StructuralParser.ProjectElement var =new ProjectElement()



            // Get Class
            var ModelClass = ea.Element as Class;
            string Basename = ModelClass.Name;
            if (!String.IsNullOrEmpty(_ViewName))
            {
                Basename = _ViewName;
            }

            foreach (CRAttribute item in ModelClass.Attributes)
            {
                if (item.Name == "CellBaseTypeAttribute")
                {
                    this._CellBaseType = GetTypeRefence(item);

                }
                if (item.Name == "EditViewBaseType")
                {
                    this._EditViewBaseType = GetTypeRefence(item);
                }
                if (item.Name == "ListViewBaseTypeAttribute")
                {
                    this._ListViewBaseType = GetTypeRefence(item);
                }
            }



            string ViewNamespace = _textDocument.ProjectElement.DefaultNamespace + ".Views";



            // Create ListView
            var ListViewClass = CreateListViewClass(ModelClass, this._ListViewBaseType, Basename);
            WriteCodeToFile(ListViewClass.WrapInFileWithImports(ViewNamespace), SourceFolder, Basename + "ListView.cs");

            var LookupListViewClass = this.CreateLookupListViewClass(ModelClass, this._ListViewBaseType, Basename);
            WriteCodeToFile(LookupListViewClass.WrapInFileWithImports(ViewNamespace), SourceFolder, Basename + "LookupListView.cs");

            var ListViewItemClass = CreateListViewCellClass(ModelClass, this._CellBaseType, Basename);
            WriteCodeToFile(ListViewItemClass.WrapInFileWithImports(ViewNamespace), SourceFolder, Basename + "ListViewCell.cs");

            var EditViewClass = CreateEditViewClass(ModelClass, this._EditViewBaseType, Basename);
            WriteCodeToFile(EditViewClass.WrapInFileWithImports(ViewNamespace), SourceFolder, Basename + "EditView.cs");

        }


        private Class CreateLookupListViewClass(Class ModelClass, TypeReferenceExpression BaseType, string ClassName)
        {
            // Prepare a few things
            string ModelName = ClassName;//ModelClass.Name;

            // Create Class
            var ListViewClass = _builder.BuildClass(ModelName + "LookupListView");
            //ListViewClass.PrimaryAncestorType = new TypeReferenceExpression("ContentPage");
            ListViewClass.PrimaryAncestorType = BaseType;
            ListViewClass.Visibility = MemberVisibility.Public;

            // Fields

            _builder.AddVariable(ListViewClass, "StackLayout", "_MainLayout");
            _builder.AddVariable(ListViewClass, "SearchBar", "_SearchBar");
            _builder.AddVariable(ListViewClass, "Xamarin.Forms.ListView", "_List");
            _builder.AddVariable(ListViewClass, GetObservableCollectionOf(ModelClass.Name), "_" + ModelClass.Name + "Collection");
            //_builder.AddVariable(ListViewClass, ModelClass.Name, "_CurrentObject");
            //_builder.AddProperty(ListViewClass, ModelClass.Name, "CurrentObject",new Get(),new Set());
            //From https://www.devexpress.com/Support/Center/Question/Details/Q486111
            Property p1 = _builder.AddProperty(ListViewClass, ModelClass.Name, "CurrentObject");
            p1.Visibility = MemberVisibility.Public;
            p1.IsAutoImplemented = true;

            // Create Default Constructor
            var ConstructorDefault = _builder.AddConstructor(ListViewClass);
            _builder.AddMethodCall(ConstructorDefault, "InitializeView");
            ConstructorDefault.Visibility = MemberVisibility.Public;

            // Create 2nd Constructor
            var Constructor = _builder.AddConstructor(ListViewClass);
            _builder.AddInParam(Constructor, GetObservableCollectionOf(ModelClass.Name).ToString(), ModelClass.Name + "Collection");
            _builder.AddAssignment(Constructor, "_" + ModelClass.Name + "Collection", ModelClass.Name + "Collection");
            _builder.AddMethodCall(Constructor, "InitializeView");
            _builder.AddAssignment(Constructor, "this._List.ItemsSource", "_" + ModelClass.Name + "Collection");
            Constructor.Visibility = MemberVisibility.Public;


            // InitializeView method
            var InitializeView = _builder.AddMethod(ListViewClass, "void", "InitializeView");
            _builder.AddAssignment(InitializeView, "BackgroundColor", "Color.White");
            _builder.AddAssignment(InitializeView, "Title", (ModelName + " List").AsString());

            _builder.AddAssignment(InitializeView, "_SearchBar", new ObjectCreationExpression(new TypeReferenceExpression("SearchBar")));
            _builder.AddAssignment(InitializeView, "_SearchBar.Placeholder", "Search".AsString());
            _builder.AddAssignment(InitializeView, "_SearchBar.BackgroundColor", "Color.White");
            _builder.AddAssignment(InitializeView, "_SearchBar.CancelButtonColor", "Color.Aqua");
            _builder.AddAssignment(InitializeView, "_SearchBar.SearchButtonPressed", new ElementReferenceExpression("Search"), AssignmentOperatorType.PlusEquals);

            _builder.AddAssignment(InitializeView, "_List", new ObjectCreationExpression(new TypeReferenceExpression("ListView")));
            _builder.AddAssignment(InitializeView, "_List.HasUnevenRows", "true");
            _builder.AddAssignment(InitializeView, "_List.SeparatorColor", "Color.Gray");

            ExpressionCollection DataTemplateArgs = new ExpressionCollection();
            TypeOfExpression CellType = new TypeOfExpression(new TypeReferenceExpression(ModelName + "ListViewCell"));
            DataTemplateArgs.Add(CellType);
            _builder.AddAssignment(InitializeView, "_List.ItemTemplate", new ObjectCreationExpression(new TypeReferenceExpression("DataTemplate"), DataTemplateArgs));
            _builder.AddAssignment(InitializeView, "_List.ItemTapped", new ElementReferenceExpression("ItemTapped"), AssignmentOperatorType.PlusEquals);

            _builder.AddAssignment(InitializeView, "_MainLayout", new ObjectCreationExpression(new TypeReferenceExpression("StackLayout")));
            _builder.AddMethodCall(InitializeView, "_MainLayout.Children.Add", new string[] { "_SearchBar" });
            _builder.AddMethodCall(InitializeView, "_MainLayout.Children.Add", new string[] { "_List" });

            _builder.AddAssignment(InitializeView, "Content", "_MainLayout");

            // Search Method
            var SearchMethod = _builder.AddMethod(ListViewClass, "void", "Search");
            _builder.AddInParam(SearchMethod, "object", "sender");
            _builder.AddInParam(SearchMethod, "EventArgs", "e");
            _builder.AddComment(SearchMethod, "TODO Implement Search", CommentType.SingleLine);
            _builder.AddComment(SearchMethod, "throw new NotImplementedException();", CommentType.SingleLine);
            //_builder.AddThrow(SearchMethod, new Throw() ,);


            // ItemTapped Method
            var ItemTappedMethod = _builder.AddMethod(ListViewClass, "void", "ItemTapped");
            _builder.AddInParam(ItemTappedMethod, "object", "sender");
            _builder.AddInParam(ItemTappedMethod, "ItemTappedEventArgs", "e");

            ExpressionCollection ConstructorArguments = new ExpressionCollection();
            TypeCastExpression CastCurrentItem = new TypeCastExpression(new TypeReferenceExpression(ModelClass.Name), new ElementReferenceExpression("e.Item"));
            //ConstructorArguments.Add(CastCurrentItem);
            ////_builder.AddInitializedVariable(ItemTappedMethod, ModelName + "EditView", ModelName + "EditView", new ObjectCreationExpression(new TypeReferenceExpression(ModelName + "EditView"), ConstructorArguments));
            //_builder.AddInitializedVariable(ItemTappedMethod, ModelName + "EditView", ModelName + "EditView", new ObjectCreationExpression(new TypeReferenceExpression(ModelName + "EditView"), ConstructorArguments));
            _builder.AddAssignment(ItemTappedMethod, "CurrentObject", CastCurrentItem);
            //_builder.AddMethodCall(ItemTappedMethod, "this.Navigation.PushAsync", new string[] { ModelName + "EditView" });
            _builder.AddMethodCall(ItemTappedMethod, "this.Navigation.PopAsync");

            return ListViewClass;
        }
        private Class CreateLookupListViewClass(Class ModelClass, string ClassName)
        {
            return CreateLookupListViewClass(ModelClass, new TypeReferenceExpression("ContentPage"), ClassName);
        }
        private Class CreateListViewClass(Class ModelClass, TypeReferenceExpression BaseType, string ClassName)
        {
            // Prepare a few things
            string ModelName = ClassName;//ModelClass.Name;

            // Create Class
            var ListViewClass = _builder.BuildClass(ModelName + "ListView");
            //ListViewClass.PrimaryAncestorType = new TypeReferenceExpression("ContentPage");
            ListViewClass.PrimaryAncestorType = BaseType;
            ListViewClass.Visibility = MemberVisibility.Public;

            // Fields

            _builder.AddVariable(ListViewClass, "StackLayout", "_MainLayout");
            _builder.AddVariable(ListViewClass, "SearchBar", "_SearchBar");
            _builder.AddVariable(ListViewClass, "Xamarin.Forms.ListView", "_List");
            _builder.AddVariable(ListViewClass, GetObservableCollectionOf(ModelClass.Name), "_" + ModelClass.Name + "Collection");

            // Create Default Constructor
            var ConstructorDefault = _builder.AddConstructor(ListViewClass);
            _builder.AddMethodCall(ConstructorDefault, "InitializeView");
            ConstructorDefault.Visibility = MemberVisibility.Public;

            // Create 2nd Constructor
            var Constructor = _builder.AddConstructor(ListViewClass);
            _builder.AddInParam(Constructor, GetObservableCollectionOf(ModelClass.Name).ToString(), ModelClass.Name + "Collection");
            _builder.AddAssignment(Constructor, "_" + ModelClass.Name + "Collection", ModelClass.Name + "Collection");
            _builder.AddMethodCall(Constructor, "InitializeView");
            _builder.AddAssignment(Constructor, "this._List.ItemsSource", "_" + ModelClass.Name + "Collection");
            Constructor.Visibility = MemberVisibility.Public;


            // InitializeView method
            var InitializeView = _builder.AddMethod(ListViewClass, "void", "InitializeView");
            _builder.AddAssignment(InitializeView, "BackgroundColor", "Color.White");
            _builder.AddAssignment(InitializeView, "Title", (ModelName + " List").AsString());

            _builder.AddAssignment(InitializeView, "_SearchBar", new ObjectCreationExpression(new TypeReferenceExpression("SearchBar")));
            _builder.AddAssignment(InitializeView, "_SearchBar.Placeholder", "Search".AsString());
            _builder.AddAssignment(InitializeView, "_SearchBar.BackgroundColor", "Color.White");
            _builder.AddAssignment(InitializeView, "_SearchBar.CancelButtonColor", "Color.Aqua");
            _builder.AddAssignment(InitializeView, "_SearchBar.SearchButtonPressed", new ElementReferenceExpression("Search"), AssignmentOperatorType.PlusEquals);

            _builder.AddAssignment(InitializeView, "_List", new ObjectCreationExpression(new TypeReferenceExpression("ListView")));
            _builder.AddAssignment(InitializeView, "_List.HasUnevenRows", "true");
            _builder.AddAssignment(InitializeView, "_List.SeparatorColor", "Color.Gray");

            ExpressionCollection DataTemplateArgs = new ExpressionCollection();
            TypeOfExpression CellType = new TypeOfExpression(new TypeReferenceExpression(ModelName + "ListViewCell"));
            DataTemplateArgs.Add(CellType);
            _builder.AddAssignment(InitializeView, "_List.ItemTemplate", new ObjectCreationExpression(new TypeReferenceExpression("DataTemplate"), DataTemplateArgs));
            _builder.AddAssignment(InitializeView, "_List.ItemTapped", new ElementReferenceExpression("ItemTapped"), AssignmentOperatorType.PlusEquals);

            _builder.AddAssignment(InitializeView, "_MainLayout", new ObjectCreationExpression(new TypeReferenceExpression("StackLayout")));
            _builder.AddMethodCall(InitializeView, "_MainLayout.Children.Add", new string[] { "_SearchBar" });
            _builder.AddMethodCall(InitializeView, "_MainLayout.Children.Add", new string[] { "_List" });

            _builder.AddAssignment(InitializeView, "Content", "_MainLayout");

            // Search Method
            var SearchMethod = _builder.AddMethod(ListViewClass, "void", "Search");
            _builder.AddInParam(SearchMethod, "object", "sender");
            _builder.AddInParam(SearchMethod, "EventArgs", "e");
            _builder.AddComment(SearchMethod, "TODO Implement Search", CommentType.SingleLine);
            _builder.AddComment(SearchMethod, "throw new NotImplementedException();", CommentType.SingleLine);
            //_builder.AddThrow(SearchMethod, new Throw() ,);


            //Add new
            var AddNew = _builder.AddMethod(ListViewClass, "void", "AddNew");
            _builder.AddInitializedVariable(AddNew, ModelName + "EditView", ModelName + "EditView", new ObjectCreationExpression(new TypeReferenceExpression(ModelName + "EditView")));
            _builder.AddMethodCall(AddNew, "this.Navigation.PushAsync", new string[] { ModelName + "EditView" });

            // ItemTapped Method
            var ItemTappedMethod = _builder.AddMethod(ListViewClass, "void", "ItemTapped");
            _builder.AddInParam(ItemTappedMethod, "object", "sender");
            _builder.AddInParam(ItemTappedMethod, "ItemTappedEventArgs", "e");

            ExpressionCollection ConstructorArguments = new ExpressionCollection();
            TypeCastExpression CurrentItemCast = new TypeCastExpression(new TypeReferenceExpression(ModelClass.Name), new ElementReferenceExpression("e.Item"));
            ConstructorArguments.Add(CurrentItemCast);
            Variable ObservableCollection = new Variable("_" + ModelClass.Name + "Collection");
            ConstructorArguments.Add(ObservableCollection);
           
            _builder.AddInitializedVariable(ItemTappedMethod, ModelName + "EditView", ModelName + "EditView", new ObjectCreationExpression(new TypeReferenceExpression(ModelName + "EditView"), ConstructorArguments));

            _builder.AddMethodCall(ItemTappedMethod, "this.Navigation.PushAsync", new string[] { ModelName + "EditView" });

            return ListViewClass;
        }
        private Class CreateListViewClass(Class ModelClass,string ClassName)
        {
            return CreateListViewClass(ModelClass, new TypeReferenceExpression("ContentPage"), ClassName);
        }
        private Class CreateListViewCellClass(Class ModelClass, string ClassName)
        {
            return CreateListViewCellClass(ModelClass, new TypeReferenceExpression("ViewCell"),ClassName);
        }
        private Class CreateListViewCellClass(Class ModelClass, TypeReferenceExpression BaseType, string ClassName)
        {   

            //TODO Implement this
            //https://developer.xamarin.com/guides/cross-platform/xamarin-forms/user-interface/listview/interactivity/
            // Prepare a few things
            string ModelName = ClassName;// ModelClass.Name;

            // Create Class
            var ListViewItemClass = _builder.BuildClass(ModelName + "ListViewCell");
            //ListViewItemClass.PrimaryAncestorType = new TypeReferenceExpression("ViewCell");
            ListViewItemClass.Visibility = MemberVisibility.Public;
            ListViewItemClass.PrimaryAncestorType = BaseType;

            // Fields

            foreach (IMemberElement Member in ModelClass.AllProperties)
            {
                _builder.AddVariable(ListViewItemClass, "Label", Member.Name + "Label");
            }
            _builder.AddVariable(ListViewItemClass, "StackLayout", "_CellLayout");

            // Create Constructor
            var Constructor = _builder.AddConstructor(ListViewItemClass);
            //TODO use override to OnBindingContextChanged() instead of constructor 
          
            Constructor.Visibility = MemberVisibility.Public;
            foreach (IMemberElement Member in ModelClass.AllProperties)
            {
                
                //_builder.AddInitializedVariable(Constructor, "Label", Member.Name + "Label", new ObjectCreationExpression(new TypeReferenceExpression("Label")));
                _builder.AddAssignment(Constructor, Member.Name + "Label", new ObjectCreationExpression(new TypeReferenceExpression("Label")));
                _builder.AddAssignment(Constructor, Member.Name + "Label.FontFamily", "HelveticaNeue - Medium".AsString());
                _builder.AddAssignment(Constructor, Member.Name + "Label.FontSize", "18");
                _builder.AddAssignment(Constructor, Member.Name + "Label.TextColor", "Color.Black");
                _builder.AddMethodCall(Constructor, Member.Name + "Label.SetBinding", new string[] { "Label.TextProperty", Member.Name.AsString() });

            }

            _builder.AddAssignment(Constructor, "_CellLayout", new ObjectCreationExpression(new TypeReferenceExpression("StackLayout")));
            _builder.AddAssignment(Constructor, "_CellLayout.Spacing", "0");
            _builder.AddAssignment(Constructor, "_CellLayout.Orientation", "StackOrientation.Vertical");
            _builder.AddAssignment(Constructor, "_CellLayout.HorizontalOptions", "LayoutOptions.FillAndExpand");

            foreach (IMemberElement Member in ModelClass.AllProperties)
            {
                _builder.AddMethodCall(Constructor, "_CellLayout.Children.Add", new string[] { Member.Name + "Label" });
            }
            _builder.AddAssignment(Constructor, "this.View", "_CellLayout");

            return ListViewItemClass;
        }
        private static string GetControlFieldName(IMemberElement Member)
        {
            return "_" + Member.Name;
        }
        private Class CreateEditViewClass(Class ModelClass, string ClassName)
        {
            return CreateEditViewClass(ModelClass, new TypeReferenceExpression("ContentPage"), ClassName);
        }
        private Class CreateEditViewClass(Class ModelClass, TypeReferenceExpression BaseType, string ClassName)
        {
            // Prepare a few things
            string ModelName = ClassName;//ModelClass.Name;

            // Create Class
            var EditViewClass = _builder.BuildClass(ModelName + "EditView");
            EditViewClass.PrimaryAncestorType = BaseType;//new TypeReferenceExpression("ContentPage");
            EditViewClass.Visibility = MemberVisibility.Public;

            // Fields

            _builder.AddVariable(EditViewClass, "StackLayout", "_MainLayout");
            _builder.AddVariable(EditViewClass, ModelClass.Name, "_" + ModelClass.Name);
            _builder.AddVariable(EditViewClass, GetObservableCollectionOf(ModelClass.Name), "_" + "Owner" + "Collection");

            foreach (IMemberElement Member in ModelClass.AllProperties)
            {
                _builder.AddVariable(EditViewClass, "Label", "_" + Member.Name + "Label");
                _builder.AddVariable(EditViewClass, GetEntryTypeOfMember((IHasType)Member), "_" + Member.Name + "Entry");
                _builder.AddVariable(EditViewClass, "StackLayout", "_" + Member.Name + "Layout");
            }
            //TODO Implment ICommands
            //foreach (IMemberElement Member in ModelClass.AllMethods)
            //{ 
            //}

            // Create Default Constructor
            var ConstructorDefault = _builder.AddConstructor(EditViewClass);
            _builder.AddMethodCall(ConstructorDefault, "InitializeView");
            ConstructorDefault.Visibility = MemberVisibility.Public;

            // Create 2nd Constructor
            var Constructor = _builder.AddConstructor(EditViewClass);
            _builder.AddInParam(Constructor, ModelClass.Name, "Instance");

            _builder.AddMethodCall(Constructor, "InitializeView");
            _builder.AddAssignment(Constructor, "_" + ModelClass.Name, "Instance");
            _builder.AddAssignment(Constructor, "this.BindingContext", "_" + ModelClass.Name);
            _builder.AddInParam(Constructor, GetObservableCollectionOf(ModelClass.Name).ToString(), "OwnerCollection");
            _builder.AddAssignment(Constructor, "_" + "OwnerCollection", "OwnerCollection");
            Constructor.Visibility = MemberVisibility.Public;


            //// Create 3rd Constructor
            //var ConstructorAddNew = _builder.AddConstructor(EditViewClass);
            //_builder.AddInParam(ConstructorAddNew, ModelClass.Name, "Instance");
            //_builder.AddMethodCall(ConstructorAddNew, "InitializeView");
            //_builder.AddAssignment(ConstructorAddNew, "this.BindingContext", "_" + ModelClass.Name);
            //ConstructorAddNew.Visibility = MemberVisibility.Public;

            foreach (IMemberElement Member in ModelClass.AllProperties)
            {
                _builder.AddMethodCall(Constructor, "_" + Member.Name + "Entry.SetBinding",
                    new string[] { GetBindingPropertyOfMember((IHasType)Member), Member.Name.AsString(), "BindingMode.TwoWay", "null", "null" });
            }

            

            

            //foreach (IMemberElement Member in ModelClass.AllProperties)
            //{
            //    _builder.AddMethodCall(Constructor, "_" + Member.Name + "Entry.SetBinding",
            //        new string[] { GetBindingPropertyOfMember((IHasType)Member), Member.Name.AsString(), "BindingMode.TwoWay", "null", "null" });
            //}

            // InitializeView method
            var InitializeView = _builder.AddMethod(EditViewClass, "void", "InitializeView");
            _builder.AddAssignment(InitializeView, "BackgroundColor", "Color.White");

            foreach (IMemberElement Member in ModelClass.AllProperties)
            {
                // Label 
                string MemberFieldName = GetControlFieldName(Member);
                IAttributeElementCollection Attributes = Member.Attributes;
                var Caption = Member.Name.AsString();
                foreach (IAttributeElement attributeElement in Attributes)
                {
                    //TODO findout why some times I need to use the full name and sometimes remove the "attribute" part
                    if (attributeElement.Name == "ViewCaptionAttribute")
                    {
                        Caption = attributeElement.Args[0].ToString() ;
                    }
                }
           
                _builder.AddAssignment(InitializeView, MemberFieldName + "Label", new ObjectCreationExpression(new TypeReferenceExpression("Label")));
                _builder.AddAssignment(InitializeView, MemberFieldName + "Label.XAlign", "TextAlignment.Start");
                _builder.AddAssignment(InitializeView, MemberFieldName + "Label.FontSize", "12");
                _builder.AddAssignment(InitializeView, MemberFieldName + "Label.TextColor", "Color.Black");
                _builder.AddAssignment(InitializeView, MemberFieldName + "Label.HorizontalOptions ", "LayoutOptions.FillAndExpand");
                //_builder.AddAssignment(InitializeView, MemberFieldName + "Label.Text", Member.Name.AsString());
                _builder.AddAssignment(InitializeView, MemberFieldName + "Label.Text", Caption);

                // Entry 
                _builder.AddAssignment(InitializeView, MemberFieldName + "Entry", new ObjectCreationExpression(new TypeReferenceExpression(GetEntryTypeOfMember((IHasType)Member))));
                //HACK Remove we should use styles insted
                //_builder.AddAssignment(InitializeView, MemberFieldName + "Entry.BackgroundColor", "Color.Gray");
                //_builder.AddAssignment(InitializeView, MemberFieldName + "Entry.TextColor", "Color.Black");
                _builder.AddAssignment(InitializeView, MemberFieldName + "Entry.HorizontalOptions", "LayoutOptions.FillAndExpand");

                // Layout
                _builder.AddAssignment(InitializeView, MemberFieldName + "Layout", new ObjectCreationExpression(new TypeReferenceExpression("StackLayout")));
                _builder.AddAssignment(InitializeView, MemberFieldName + "Layout.Orientation", "StackOrientation.Vertical");
                _builder.AddAssignment(InitializeView, MemberFieldName + "Layout.HorizontalOptions", "LayoutOptions.FillAndExpand");

                _builder.AddMethodCall(InitializeView, MemberFieldName + "Layout.Children.Add", new string[] { MemberFieldName + "Label" });
                _builder.AddMethodCall(InitializeView, MemberFieldName + "Layout.Children.Add", new string[] { MemberFieldName + "Entry" });
            }

            _builder.AddAssignment(InitializeView, "_MainLayout", new ObjectCreationExpression(new TypeReferenceExpression("StackLayout")));

            _builder.AddAssignment(InitializeView, "_MainLayout.Spacing", "0");
            ExpressionCollection newExpressionCollection = new ExpressionCollection();
            newExpressionCollection.Add(new PrimitiveExpression("10"));
            newExpressionCollection.Add(new PrimitiveExpression("5"));
            newExpressionCollection.Add(new PrimitiveExpression("10"));
            newExpressionCollection.Add(new PrimitiveExpression("5"));
            _builder.AddAssignment(InitializeView, "_MainLayout.Padding", new ObjectCreationExpression(new TypeReferenceExpression("Thickness"), newExpressionCollection));

            _builder.AddAssignment(InitializeView, "_MainLayout.Orientation", "StackOrientation.Vertical");
            _builder.AddAssignment(InitializeView, "_MainLayout.HorizontalOptions", "LayoutOptions.FillAndExpand");

            foreach (IMemberElement Member in ModelClass.AllProperties)
            {
               // _builder.AddMethodCall(InitializeView, "_MainLayout.Children.Add", new string[] { "_" + Member.Name + "Layout" });
                _builder.AddMethodCall(InitializeView, "_MainLayout.Children.Add", new string[] { GetControlFieldName(Member)  + "Layout" });
            }
            _builder.AddAssignment(InitializeView, "Content", "_MainLayout");

            return EditViewClass;
        }

        private static TypeReferenceExpression GetObservableCollectionOf(string ModelName)
        {
            TypeReferenceExpression CollectionType = new TypeReferenceExpression("ObservableCollection");
            CollectionType.AddTypeArgument(new TypeReferenceExpression(ModelName));
            return CollectionType;
        }
        private string GetEntryTypeOfMember(IHasType Member)
        {
            switch (Member.Type.Name)
            {
                case "bool": return "Switch";
                case "DateTime": return "DatePicker";
                case "TimeSpan": return "TimePicker";
                case "string": return "Entry";
                default: return "Entry";
            }
        }

        private string GetBindingPropertyOfMember(IMemberElement Member)
        {

            IHasType IHasType = (IHasType)Member;
            foreach (IAttributeElement item in Member.Attributes)
            {
                
            }

            switch (IHasType.Type.Name)
            {
                case "bool": return "Switch.IsToggledProperty";
                case "DateTime": return "DatePicker.DateProperty";
                case "TimeSpan": return "TimeProperty";
                
                default: return "Entry.TextProperty";
            }
        }
        private string GetBindingPropertyOfMember(IHasType Member)
        {
           
            switch (Member.Type.Name)
            {
                case "bool": return "Switch.IsToggledProperty";
                case "DateTime": return "DatePicker.DateProperty";
                case "TimeSpan": return "TimeProperty";
                
                default: return "Entry.TextProperty";
            }
        }
        private void WriteCodeToFile(LanguageElement CodeObject, string folder, string filename)
        {
            var filePath = Path.Combine(folder, filename);

            // if File exists, Exit
            if (File.Exists(filePath))
                return;

            // Create File
            File.WriteAllText(filePath, CodeObject.GenerateCode());

            // Add File to Project
            CodeRush.Project.AddFile(_textDocument.ProjectElement, filePath);

        }
        //JM
    
    
    
    
    
    
    }
}