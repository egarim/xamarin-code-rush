﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tinker.CodeRush.Notation.XamarinSimpleMVVM;
using Xamarin.Forms;

namespace XGeneratorTest
{
    public class BasePage:ContentPage
    {
        
        public BasePage()
        {
            
        }
    }
    [ViewModel(ViewName="JocheView")]
    [CellBaseTypeAttribute(typeof(TextCell))]
    [EditViewBaseType(typeof(BasePage))]
    public class CustomerViewModel
    {
        // Fields...
        private decimal _DueAmmout;
        private bool _Active;
        private string _Phone;
        private string _Name;

        [ViewCaptionAttribute("Customer Name")]
        public string name
        {
            get { return _Name; }
            set
            {
                _Name = value;
            }
        }

        // Fields...
        private string _Address;

        public string Address
        {
            get { return _Address; }
            set
            {
                _Address = value;
            }
        }


        public string Phone
        {
            get { return _Phone; }
            set
            {
                _Phone = value;
            }
        }

        public bool Active
        {
            get { return _Active; }
            set
            {
                _Active = value;
            }
        }

        public decimal DueAmmout
        {
            get { return _DueAmmout; }
            set
            {
                _DueAmmout = value;
            }
        }

    }
}
