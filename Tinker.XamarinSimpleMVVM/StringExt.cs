using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.CodeRush.Core;
using DevExpress.CodeRush.StructuralParser;
using System.IO;

namespace Tinker.XamarinSimpleMVVM
{
    public static class StringExt
    {
        public static string AsString(this string source)
        {
            return "\"" + source + "\"";
        }
        public static string GenerateCode(this LanguageElement le)
        {
            return CodeRush.CodeMod.GenerateCode(le, false);
        }
        public static LanguageElement WrapInFileWithImports(this LanguageElement le, string Namespace)
        {
            var root = new SourceFile();
            root.AddNode(new NamespaceReference("System"));
            root.AddNode(new NamespaceReference("System.Collections.ObjectModel"));
            root.AddNode(new NamespaceReference("Xamarin.Forms"));
            Namespace TheNameSpace = new Namespace(Namespace);
            root.AddNode(TheNameSpace);
            TheNameSpace.AddNode(le);
            return root;
        }
        public static string GetFolderName(this TextDocument textDocument)
        {
            return new FileInfo(textDocument.FullName).DirectoryName;
        }
    }
}
