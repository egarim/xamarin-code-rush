﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tinker.CodeRush.Notation.XamarinSimpleMVVM
{
    public class EditorTypeAttribute:Attribute
    {
        EditorTypes _EditorType;
        /// <summary>
        /// Initializes a new instance of the <see cref="EditorTypeAttribute"/> class.
        /// </summary>
        /// <param name="editorType"></param>
        public EditorTypeAttribute(EditorTypes editorType)
        {
            _EditorType = editorType;
        }
    }
    public enum EditorTypes
    {
        Entry=0,Editor=1,
    }
}
