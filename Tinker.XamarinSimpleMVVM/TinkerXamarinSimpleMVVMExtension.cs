﻿using System.ComponentModel.Composition;
using DevExpress.CodeRush.Common;

namespace Tinker.XamarinSimpleMVVM
{
  [Export(typeof(IVsixPluginExtension))]
  public class TinkerXamarinSimpleMVVMExtension : IVsixPluginExtension {}
}