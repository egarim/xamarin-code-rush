﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tinker.CodeRush.Notation.XamarinSimpleMVVM
{
    public class ViewModelAttribute:Attribute
    {
        // Fields...
        private string _ViewName;

        public string ViewName
        {
            get { return _ViewName; }
            set
            {
                _ViewName = value;
            }
        }
         
    }
}
