﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tinker.CodeRush.Notation.XamarinSimpleMVVM
{
    public class ViewCaptionAttribute:Attribute
    {
        // Fields...
        private string _Caption;

        public string Caption
        {
            get { return _Caption; }
            set
            {
                _Caption = value;
            }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewCaptionAttribute"/> class.
        /// </summary>
        /// <param name="caption"></param>
        public ViewCaptionAttribute(string caption)
        {
            _Caption = caption;
        }
         
    }
}
